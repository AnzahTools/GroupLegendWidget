<?php

namespace AnzahTools\GroupLegendWidget;

use XF\AddOn\AbstractSetup;
use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
	public function install(array $stepParams = [])
	{
		$sm = $this->schemaManager();

		$sm->alterTable('xf_user_group', function (Alter $table)
		{
				$table->addColumn('at_glw_display', 'bool')->setDefault(0);
		});
	}

	public function upgrade(array $stepParams = [])
	{
		if ($this->addOn->version_id < 1030070)
		{
				$sm = $this->schemaManager();

				$sm->alterTable('xf_user_group', function (Alter $table)
				{
						$table->addColumn('at_glw_display', 'bool')->setDefault(0);
				});
		}
	}

	public function uninstall(array $stepParams = [])
	{
		$sm = \XF::db()->getSchemaManager();

		$sm->alterTable('xf_user_group', function (Alter $table)
		{
				$table->dropColumns('at_glw_display');
		});
	}
}
