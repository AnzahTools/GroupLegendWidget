<?php

namespace AnzahTools\GroupLegendWidget\XF\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * @property bool at_promote_primaryGroupSelect
 * @property bool at_promote_secondaryGroupSelect
 */
class UserGroup extends XFCP_UserGroup
{

    public static function getStructure(Structure $structure)
    {
        $structure = parent::getStructure($structure);

        $structure->columns['at_glw_display'] = ['type' => Entity::BOOL, 'default' => 0];

        return $structure;
    }
}
