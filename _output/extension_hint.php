<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace AnzahTools\GroupLegendWidget\XF\Admin\Controller
{
	class XFCP_UserGroup extends \XF\Admin\Controller\UserGroup {}
}

namespace AnzahTools\GroupLegendWidget\XF\Entity
{
	class XFCP_UserGroup extends \XF\Entity\UserGroup {}
}