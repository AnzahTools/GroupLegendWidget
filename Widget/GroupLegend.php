<?php
namespace AnzahTools\GroupLegendWidget\Widget;

use XF\Widget\AbstractWidget;

/**
 * Class GroupLegend
 *
 * @package AnzahTools\GroupLegend\Widget
 */
class GroupLegend extends AbstractWidget
{
	protected $defaultOptions = [
		'at_glw_linknames' => false,
		'at_glw_cssOption' => 'banner_css',
		'at_glw_title' => 'banner_text'
	];

	public function render()
	{
		$options = $this->options;

		$finder = \XF::finder('XF:UserGroup')->where(
				[
						'at_glw_display' => true
				]
		)->order('display_style_priority','DESC');

		$userGroups = $finder->fetch();
		$outputGroups = [];

		foreach($userGroups as $group)
		{
			array_push($outputGroups,[
				'username_css' => $group['username_css'],
				'group_title' => $group['title'],
				'banner_text' => $group['banner_text'],
				'user_title' => $group['user_title'],
				'banner_css_class' => $group['banner_css_class']
			]);
		}

		$viewParams = [
				'outputGroups' => $outputGroups
		];

		return $this->renderer('at_glw_widget',$viewParams);
	}

	public function verifyOptions(\XF\Http\Request $request, array &$options, &$error = null)
	{
		$options = $request->filter([
			'at_glw_linknames' => 'bool',
			'at_glw_cssOption' => 'str',
			'at_glw_title' => 'str'
		]);
		return true;
	}
}
